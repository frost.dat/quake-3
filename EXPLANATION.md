# Сборка quake 3 server в docker

## Сборка

Итак, цели ясны - можем приступать к задаче.
В этом файле я буду описывать все шаги к выполнению задачи, а так же объяснять как и почему я поступил
при ее реализации.

Первое с чем необходимо определиться - выбрать базовый образ.
Выбор упал на <code>ubuntu:20.04</code>, почему? Все очень просто - мне так захотелось, 
тем более в ТЗ не было условий выбора базового образа.

Собирать все будем из исходных файлов.
Мой взор упал на один интересный источник - [ioquake3](https://ioquake3.org/), 
который, к моему счастью, имеет свой репозиторий на [GitHub](https://github.com/ioquake/ioq3).
Долго и упорно изучая репозиторий я наткнулся на два великолепных скрипта, один из которых скомпилирует наш сервер,
а другой - запустит его, ведь это то что нам нужно!
```
https://github.com/ioquake/ioq3/blob/master/misc/linux/server_compile.sh
```
Заходим в тело скрипта и видим несколько интересных и важных для нас моментов.
Первый момент - передаваемые параметры при компиляции скрипта. 
```
export BUILD_CLIENT="${BUILD_CLIENT:-0}"
export BUILD_SERVER="${BUILD_SERVER:-1}"
export USE_CURL="${USE_CURL:-1}"
export USE_CODEC_OPUS="${USE_CODEC_OPUS:-1}"
export USE_VOIP="${USE_VOIP:-1}"
export COPYDIR="${COPYDIR:-~/ioquake3}"
IOQ3REMOTE="${IOQ3REMOTE:-https://github.com/ioquake/ioq3.git}"
MAKE_OPTS="${MAKE_OPTS:--j2}"
```
<code>IOQ3REMOTE</code> - нас вполне устраивает, а вот параметр <code>COPYDIR</code> мы можем изменить.
Авторы репозитория любезно подсказывают нам разместить файлы игры в <code>/usr/local/games/</code>, последуем их бесценному совету.
Второй момент - проверка на наличие установленных пакетов в системе (git и make). Примем это к сведению!
```
if ! [ -x "$(command -v git)" ] || ! [ -x "$(command -v make)" ]; then
        echo "This build script requires 'git' and 'make' to be installed." >&2
        echo "Please install them through your normal package installation system." >&2
        exit 1
fi
```
Исходя из этого, в <code>Dockerfile</code> мы добавим следующие пакеты:
```
apt-get install git build-essential -y \
```
Третий момент - цикл <code>while</code>, который работает в интерактивном режиме, а это означает что нам необходимо передавать "Y",
чтобы наш скрипт корректно отработал.
```
while true; do
        read -p "Are you ready to compile ioquake3 from ${IOQ3REMOTE}, and have it installed into $COPYDIR? " yn
        case $yn in
                [Yy]*)
                        git clone $IOQ3REMOTE $BUILD_DIR/ioq3
                        cd $BUILD_DIR/ioq3
                        make $MAKE_OPTS
                        make copyfiles
                        exit
                        ;;
                [Nn]*)
                        echo "aborting installation."
                        exit
                        ;;
                *)
                        echo "Please answer yes or no."
                        ;;
        esac
done
```

Мы можем использовать скрипт с [Git](https://github.com/ioquake/ioq3/raw/master/misc/linux/server_compile.sh), 
однако удобнее будет его поменять, что и сделаем.
Поменяв параметр <code>COPYDIR</code> и убрав интерактивный режим, скопируем наш скрипт в контейнер и попробуем скомпилировать файлы.
После компиляции запускаем наш контейнер предложенной командой из 
[скрипта для старта сервера](https://github.com/ioquake/ioq3/raw/master/misc/linux/start_server.sh):
```
Edit this script to change the path to ioquake3's dedicated server executable and which binary if you aren't on x86_64.
Set the sv_dlURL setting to a url like http://yoursite.com/ioquake3_path for ioquake3 clients to download extra data.
ioq3 1.36_GIT_f2c61c14-2020-02-11 linux-x86_64 Apr 26 2020
SSE instruction set enabled
----- FS_Startup -----
We are looking in the current search path:
/root/.q3a/baseq3
/root/ioquake3/baseq3
 
----------------------
0 files in pk3 files
"pak0.pk3" is missing.
Please copy it from your legitimate Q3 CDROM.
Point Release files are missing.
Please re-install the 1.32 point release.
Also check that your ioq3 executable is in the correct place and that every file in the "baseq3" directory is present and readable
```
Похоже что-то пошло не так...
Анализируем текст и заходим на [вики](https://ru.wikipedia.org/wiki/Ioquake3). Видим:
```
Для работы ioquake3 необходим несвободный файл данных из оригинальной игры, 
pak0.pk3, который можно взять с установочного компакт-диска Quake III Arena, 
приобретя игру с помощью сервиса Steam и во многих других магазинах.
Остальные файлы данных, pak1.pk3, pak2.pk3 и другие, можно легально загрузить из Интернета
(например, с официального сайта ioquake3), потому что эти файлы распространяются вместе с патчами к игре. 
```
Скачиваем необходимые файлы <code>pak0.pk3</code> и [остальные нужные вещи](https://www.ioquake3.org/data/quake3-latest-pk3s.zip) 
в корень нашего <code>Dockerfile</code> и закидываем их в образ.
```
COPY /quake3-latest-pk3s/baseq3 /usr/local/games/ioquake3/baseq3
COPY /quake3-latest-pk3s/missionpack /usr/local/games/ioquake3/missionpack
```
Попробуем запустить наш сервер.
Согласно [README.md](https://github.com/ioquake/ioq3/raw/master/README.md) 
будем запускать наш сервер со следующими параметрами:
```
sh /usr/local/games/ioquake3/ioq3ded.x86_64 +set netip 127.0.0.1 +set net_enabled 1 +set dedicated 2
```
Полученный результат:
```
ioq3 1.36_GIT_f2c61c14-2020-02-11 linux-x86_64 Apr 26 2020
SSE instruction set enabled
----- FS_Startup -----
We are looking in the current search path:
/root/.q3a/baseq3
/root/ioquake3/baseq3
/root/ioquake3/baseq3/pak8.pk3 (9 files)
/root/ioquake3/baseq3/pak7.pk3 (4 files)
/root/ioquake3/baseq3/pak6.pk3 (64 files)
/root/ioquake3/baseq3/pak5.pk3 (7 files)
/root/ioquake3/baseq3/pak4.pk3 (272 files)
/root/ioquake3/baseq3/pak3.pk3 (4 files)
/root/ioquake3/baseq3/pak2.pk3 (148 files)
/root/ioquake3/baseq3/pak1.pk3 (26 files)
/root/ioquake3/baseq3/pak0.pk3 (3539 files)
 
----------------------
4073 files in pk3 files
execing default.cfg
couldn't exec q3config_server.cfg
couldn't exec autoexec.cfg
Hunk_Clear: reset the hunk ok
--- Common Initialization Complete ---
IP: 127.0.0.1
IP: 172.17.0.2
Opening IP socket: 0.0.0.0:27960
```
Как видим наш сервер просит конфигурационные файлы.
К сожалению официальная вики ioquake3.org по каким-то причинам не работает, поэтому ищем 
[альтренативы](https://www.quake3world.com/q3guide/servers.html).

Создадим файлы конфигурации <code>q3config_server.cfg</code> и <code>autoexec.cfg</code>.
Чтобы не писать сложные конструкции при запуске сервера, запишем все это в <code>autoexec.cfg</code>, 
которые будут выполнятся при инициализации сервера.
В файле <code>q3config_server.cfg</code> будут хранится настройки сервера.
Пробуем завести сервер: 
```
ioq3 1.36_GIT_f2c61c14-2020-02-11 linux-x86_64 Apr 26 2020
SSE instruction set enabled
----- FS_Startup -----
We are looking in the current search path:
/root/.q3a/baseq3
/root/ioquake3/baseq3
/root/ioquake3/baseq3/pak8.pk3 (9 files)
/root/ioquake3/baseq3/pak7.pk3 (4 files)
/root/ioquake3/baseq3/pak6.pk3 (64 files)
/root/ioquake3/baseq3/pak5.pk3 (7 files)
/root/ioquake3/baseq3/pak4.pk3 (272 files)
/root/ioquake3/baseq3/pak3.pk3 (4 files)
/root/ioquake3/baseq3/pak2.pk3 (148 files)
/root/ioquake3/baseq3/pak1.pk3 (26 files)
/root/ioquake3/baseq3/pak0.pk3 (3539 files)
 
----------------------
4073 files in pk3 files
execing default.cfg
execing q3config_server.cfg
execing autoexec.cfg
execing q3config_server.cfg
Hunk_Clear: reset the hunk ok
--- Common Initialization Complete ---
IP: 127.0.0.1
IP: 172.17.0.2
Opening IP socket: 0.0.0.0:27960
execing autoexec.cfg
execing q3config_server.cfg
```
Отлично! Все работает!
Запускать сервер будем от имени непривилигированного пользователя <code>q3-user</code>, 
соответственно предварительно его создав. Команда на запуск сервера будет выглядить следующим образом:
```
CMD ["/usr/local/games/ioquake3/ioq3ded.x86_64", "+exec", "autoexec.cfg"]
```
Теперь нам нужно определиться с портами, исходя из этой статьи на 
[вики](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BF%D0%BE%D1%80%D1%82%D0%BE%D0%B2_TCP_%D0%B8_UDP) 
мы будем использовать 27960
```
EXPOSE 27960
```
## Важное замечание!
Для корректного запуска контейнера необходимо выполнить команду:
```
docker run -d -p 0.0.0.0:27960:27960/udp registry.gitlab.com/frost.dat/quake-3:latest
```