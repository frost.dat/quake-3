FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install git build-essential -y \
    && apt-get clean

COPY server_compile.sh /tmp/

RUN cd /tmp/ \
    && chmod +x server_compile.sh \
    && sh /tmp/server_compile.sh \
    && adduser q3-user

COPY /quake3-latest-pk3s/baseq3 /usr/local/games/ioquake3/baseq3
COPY /quake3-latest-pk3s/missionpack /usr/local/games/ioquake3/missionpack
COPY autoexec.cfg /usr/local/games/ioquake3/baseq3
COPY q3config_server.cfg /usr/local/games/ioquake3/baseq3
COPY map.cfg /usr/local/games/ioquake3/baseq3

USER q3-user

EXPOSE 27960

CMD ["/usr/local/games/ioquake3/ioq3ded.x86_64", "+exec", "autoexec.cfg"]
